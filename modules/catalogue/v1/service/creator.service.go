package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"grpc-starter/common/config"
	"grpc-starter/modules/catalogue/v1/entity"
	"grpc-starter/modules/catalogue/v1/internal/repository"
)

type LibraryCreatorService struct {
	config   config.Config
	bookRepo repository.BookRepositoryUseCase
}

type BookCreatorUseCase interface {
	CreateBook(ctx context.Context, publisherId, authorId uuid.UUID, title, isbn string) (*entity.Book, error)
}

func NewLibraryCreatorService(
	config config.Config,
	bookRepo repository.BookRepositoryUseCase) *LibraryCreatorService {
	return &LibraryCreatorService{
		config:   config,
		bookRepo: bookRepo,
	}
}

func (service *LibraryCreatorService) CreateBook(ctx context.Context, publisherId, authorId uuid.UUID, title, isbn string) (*entity.Book, error) {
	newBook := entity.NewBook(uuid.New(), publisherId, authorId, title, isbn)
	if err := service.bookRepo.CreateBook(ctx, newBook); err != nil {
		return nil, err
	}
	return newBook, nil
}
