package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"grpc-starter/common/config"
	"grpc-starter/common/errors"
	"grpc-starter/modules/catalogue/v1/internal/repository"
)

type LibraryDeleterService struct {
	config   config.Config
	bookRepo repository.BookRepositoryUseCase
}

type BookDeleterUseCase interface {
	DeleteBook(ctx context.Context, authorId uuid.UUID, bookId uuid.UUID) error
}

func NewLibraryDeleterService(
	config config.Config,
	bookRepo repository.BookRepositoryUseCase) *LibraryDeleterService {
	return &LibraryDeleterService{
		config:   config,
		bookRepo: bookRepo,
	}
}

func (service *LibraryDeleterService) DeleteBook(ctx context.Context, authorId uuid.UUID, bookId uuid.UUID) error {
	result, err := service.bookRepo.GetBookById(ctx, bookId)
	if err != nil {
		return err
	}

	if result.UserId != authorId {
		return errors.NewError(codes.PermissionDenied, "forbidden to delete book other than the author of the book").Error()
	}

	if err := service.bookRepo.DeleteBook(ctx, bookId); err != nil {
		return err
	}
	return nil
}
