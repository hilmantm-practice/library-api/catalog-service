package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"grpc-starter/common/config"
	"grpc-starter/common/errors"
	"grpc-starter/modules/catalogue/v1/entity"
	"grpc-starter/modules/catalogue/v1/internal/repository"
)

type LibraryUpdaterService struct {
	config   config.Config
	bookRepo repository.BookRepositoryUseCase
}

type BookUpdaterUseCase interface {
	UpdateBook(ctx context.Context, authorId uuid.UUID, book *entity.Book) error
}

func NewLibraryUpdaterService(
	config config.Config,
	bookRepo repository.BookRepositoryUseCase) *LibraryUpdaterService {
	return &LibraryUpdaterService{config: config, bookRepo: bookRepo}
}

func (service *LibraryUpdaterService) UpdateBook(ctx context.Context, authorId uuid.UUID, book *entity.Book) error {
	result, err := service.bookRepo.GetBookById(ctx, book.ID)
	if err != nil {
		return err
	}

	if result.UserId != authorId {
		return errors.NewError(codes.PermissionDenied, "forbidden to change data other than the author of the book").Error()
	}

	if err := service.bookRepo.UpdateBook(ctx, book); err != nil {
		return err
	}

	return nil
}
