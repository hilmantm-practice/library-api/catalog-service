package builder

import (
	"gorm.io/gorm"
	"grpc-starter/common/config"
	"grpc-starter/modules/catalogue/v1/internal/grpc/handler"
	"grpc-starter/modules/catalogue/v1/internal/repository"
	"grpc-starter/modules/catalogue/v1/service"
)

func BuildBookHandler(config config.Config, db *gorm.DB) *handler.BookHandler {
	// provide repo
	bookRepo := repository.NewBookRepository(db)

	// provide service
	creatorUseCase := service.NewLibraryCreatorService(config, bookRepo)
	finderUseCase := service.NewLibraryFinderService(config, bookRepo)
	updaterUseCase := service.NewLibraryUpdaterService(config, bookRepo)
	deleterUseCase := service.NewLibraryDeleterService(config, bookRepo)

	return handler.NewBookHandler(config, creatorUseCase, finderUseCase, updaterUseCase, deleterUseCase)
}
