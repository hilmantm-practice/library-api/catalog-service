package handler

import (
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	bookv1 "grpc-starter/api/book/v1"
	"grpc-starter/common/config"
	"grpc-starter/common/errors"
	"grpc-starter/modules/catalogue/v1/entity"
	"grpc-starter/modules/catalogue/v1/service"
)

type BookHandler struct {
	bookv1.UnimplementedCatalogServiceServer
	config         config.Config
	creatorUseCase service.BookCreatorUseCase
	finderUseCase  service.BookFinderUseCase
	updaterUseCase service.BookUpdaterUseCase
	deleterUseCase service.BookDeleterUseCase
}

func NewBookHandler(
	config config.Config,
	creatorUseCase service.BookCreatorUseCase,
	finderUseCase service.BookFinderUseCase,
	updaterUseCase service.BookUpdaterUseCase,
	deleterUseCase service.BookDeleterUseCase) *BookHandler {
	return &BookHandler{config: config, creatorUseCase: creatorUseCase, finderUseCase: finderUseCase, updaterUseCase: updaterUseCase, deleterUseCase: deleterUseCase}
}

func (handler *BookHandler) CreateBook(ctx context.Context, request *bookv1.CreateBookRequest) (*bookv1.CommandBookResponse, error) {
	publisherId, err := uuid.Parse(request.PublisherId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	authorId, err := uuid.Parse(request.AuthorId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	createdBook, err := handler.creatorUseCase.CreateBook(ctx, publisherId, authorId, request.Title, request.Isbn)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	return &bookv1.CommandBookResponse{Code: 201, Message: "Book Created", Data: &bookv1.BookResponse{
		AuthorId:    createdBook.UserId.String(),
		PublisherId: createdBook.PublisherId.String(),
		Title:       createdBook.Title,
		Isbn:        createdBook.ISBN,
		CreatedAt:   createdBook.CreatedAt.String(),
	}}, nil
}

func (handler *BookHandler) GetBooksByAuthor(ctx context.Context, request *bookv1.BooksByAuthorRequest) (*bookv1.BooksResponse, error) {
	if request.GetAuthorId() == "00000000-0000-0000-0000-000000000000" {
		parseError := errors.NewError(codes.InvalidArgument, "required author information")
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	authorId, err := uuid.Parse(request.AuthorId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	res, err := handler.finderUseCase.GetBooksByAuthor(ctx, authorId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	var books []*bookv1.BookResponse
	for _, book := range res {
		books = append(books, &bookv1.BookResponse{
			AuthorId:    book.UserId.String(),
			PublisherId: book.PublisherId.String(),
			Title:       book.Title,
			Isbn:        book.ISBN,
			CreatedAt:   book.CreatedAt.String(),
		})
	}

	return &bookv1.BooksResponse{
		Code:    200,
		Message: "success get books by author",
		Data:    books,
	}, nil
}

func (handler *BookHandler) GetAllBooks(ctx context.Context, request *bookv1.AllBooksRequest) (*bookv1.BooksResponse, error) {
	res, err := handler.finderUseCase.GetAllBooks(ctx)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	var books []*bookv1.BookResponse
	for _, book := range res {
		books = append(books, &bookv1.BookResponse{
			AuthorId:    book.UserId.String(),
			PublisherId: book.PublisherId.String(),
			Title:       book.Title,
			Isbn:        book.ISBN,
			CreatedAt:   book.CreatedAt.String(),
		})
	}

	return &bookv1.BooksResponse{
		Code:    200,
		Message: "success get all books",
		Data:    books,
	}, nil
}

func (handler *BookHandler) UpdateBook(ctx context.Context, request *bookv1.UpdateBookRequest) (*bookv1.CommandBookResponse, error) {
	bookId, err := uuid.Parse(request.BookId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	publisherId, err := uuid.Parse(request.PublisherId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	authorId, err := uuid.Parse(request.AuthorId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	book := entity.NewBook(bookId, publisherId, authorId, request.Title, request.Isbn)
	err = handler.updaterUseCase.UpdateBook(ctx, authorId, book)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	return &bookv1.CommandBookResponse{
		Code:    200,
		Message: "success to update book data",
		Data: &bookv1.BookResponse{
			AuthorId:    book.UserId.String(),
			PublisherId: book.PublisherId.String(),
			Title:       book.Title,
			Isbn:        book.ISBN,
			CreatedAt:   book.CreatedAt.String(),
		},
	}, nil
}

func (handler *BookHandler) DeleteBook(ctx context.Context, request *bookv1.DeleteBookRequest) (*bookv1.CommandBookResponse, error) {
	bookId, err := uuid.Parse(request.BookId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	authorId, err := uuid.Parse(request.AuthorId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	err = handler.deleterUseCase.DeleteBook(ctx, authorId, bookId)
	if err != nil {
		parseError := errors.ParseError(err)
		return nil, status.Errorf(
			parseError.Code,
			parseError.Message,
		)
	}

	return &bookv1.CommandBookResponse{
		Code:    200,
		Message: "success to delete book data",
		Data:    nil,
	}, nil
}
