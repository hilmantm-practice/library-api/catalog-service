package entity

import (
	"github.com/google/uuid"
	"time"
)

const BOOKS_TABLE = "catalogue.books"

type Book struct {
	ID          uuid.UUID `json:"id"`
	PublisherId uuid.UUID `json:"publisher_id"`
	UserId      uuid.UUID `json:"user_id"` // This is author
	Title       string    `json:"title"`
	ISBN        string    `json:"isbn"`
	Auditable
}

// TableName specifies table name
func (model *Book) TableName() string {
	return BOOKS_TABLE
}

func NewBook(id uuid.UUID, publisherId, userId uuid.UUID, title, isbn string) *Book {
	return &Book{
		ID:          id,
		PublisherId: publisherId,
		UserId:      userId,
		Title:       title,
		ISBN:        isbn,
		Auditable:   NewAuditable("author"),
	}
}

func (model *Book) MapUpdateFrom(from *Book) *map[string]interface{} {
	if from != nil {
		return &map[string]interface{}{
			"publisher_id": from.PublisherId,
			"title":        from.Title,
			"isbn":         from.ISBN,
			"updated_at":   from.UpdatedAt,
		}
	}

	mapped := make(map[string]interface{})

	if model.PublisherId != from.PublisherId {
		mapped["publisher_id"] = from.PublisherId
	}

	if model.Title != from.Title {
		mapped["title"] = from.Title
	}

	if model.ISBN != from.ISBN {
		mapped["isbn"] = from.ISBN
	}

	mapped["updated_at"] = time.Now()

	return &mapped

}
