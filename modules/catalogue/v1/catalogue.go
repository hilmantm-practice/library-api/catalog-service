package v1

import (
	"google.golang.org/grpc"
	"gorm.io/gorm"
	bookv1 "grpc-starter/api/book/v1"
	"grpc-starter/common/config"
	"grpc-starter/modules/catalogue/v1/internal/builder"
)

func InitGRPCCatalogue(server *grpc.Server, cfg config.Config, db *gorm.DB) {
	bookHandler := builder.BuildBookHandler(cfg, db)
	bookv1.RegisterCatalogServiceServer(server, bookHandler)
}
