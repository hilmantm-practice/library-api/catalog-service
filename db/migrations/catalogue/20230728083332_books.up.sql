BEGIN;

CREATE TABLE IF NOT EXISTS catalogue.books
(
    id                    UUID         NOT NULL,
    publisher_id          UUID         NOT NULL,
    user_id               UUID         NOT NULL,
    title                 VARCHAR(255) NOT NULL,
    isbn                  VARCHAR(255) NOT NULL,
    created_at            TIMESTAMPTZ  NOT NULL,
    updated_at            TIMESTAMPTZ  NULL,
    deleted_at            TIMESTAMPTZ  NULL,
    PRIMARY KEY (id)
);

COMMIT;